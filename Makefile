docker_build:
	docker build . -t marsrover

docker_run:
	docker run --publish 8080:8080 marsrover

test:
	go clean -testcache && go test ./... -v