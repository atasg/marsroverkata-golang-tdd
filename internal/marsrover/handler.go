package marsrover

import (
	"github.com/gofiber/fiber/v2"
)

type Handler struct{
	Service HandlerService
}

type HandlerService interface {
	GetResult(r *MarsRoverRequest) *Response
}

func NewHandler(s HandlerService) *Handler{
	return &Handler{
		Service: s,
	}
}

func (h *Handler) MarsRoverRouters(route fiber.Router){
	route.Get("/newcoordinate", h.GetResult)
	route.Get("/health", h.HealthCheck)
}

func (h *Handler) SetupRoutes(app *fiber.App) {
    app.Get("/", func(c *fiber.Ctx) error {
        return c.Status(fiber.StatusOK).JSON(fiber.Map{
            "success":     true,
            "message":     "Root Endpoint",
        })
    })

    h.MarsRoverRouters(app.Group("/marsrover"))
}

func (h *Handler) HealthCheck(c *fiber.Ctx) error{
	return c.SendStatus(fiber.StatusOK)
}


func (h *Handler) GetResult(c *fiber.Ctx) error{
	var request MarsRoverRequest
	
    if err := c.BodyParser(&request); err != nil {
        return c.Status(400).JSON(fiber.Map{
            "success": false,
            "message": "Failed to parse body",
        })
    }

	response := h.Service.GetResult(&request)

	if response.IsThereError{
		return c.Status(fiber.StatusUnprocessableEntity).JSON(response.ErrorResponse)
	}

	return c.Status(fiber.StatusOK).JSON(response.MarsRoverResponse)
}