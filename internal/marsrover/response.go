package marsrover

type ( 
	MarsRoverResponse struct {
		Coordinate Coordinate `json:"coordinate"`
		Message           string     `json:"message"`
	}

	ErrorResponse struct{
		Error string `json:"error"`
	}

	Response struct{
		MarsRoverResponse MarsRoverResponse `json:"marsroverresponse"`
		ErrorResponse ErrorResponse `json:"errorresponse"`
		IsThereError bool `json:"obstacleoninitialcoordinate"`
	}
)