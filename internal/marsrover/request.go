package marsrover

type (
	MarsRoverRequest struct {
		Coordinate Coordinate `json:"coordinate"`
		Commands   []string   `json:"commands"`
	}

	Point struct {
		X int64 `json:"x"`
		Y int64 `json:"y"`
	}
	
	Coordinate struct {
		Point     Point  `json:"point"`
		Direction string `json:"direction"`
	}
)