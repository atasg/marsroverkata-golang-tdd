package marsrover_test

import (
	"strconv"
	"testing"
	"marsRover/internal/marsrover"
	"github.com/stretchr/testify/assert"
)

func TestMoveForward(t *testing.T){
	s := marsrover.NewService()

	areaTestsNotEdgePoints:= []struct{
		direction string
		startingCoordinate marsrover.Coordinate
		finalCoordinate marsrover.Coordinate
	}{
		{direction:"E", startingCoordinate: marsrover.Coordinate{Point:marsrover.Point{X:2,Y:3}, Direction:"E"}, finalCoordinate: marsrover.Coordinate{Point: marsrover.Point{X:3,Y:3}, Direction: "E"}},
		{direction:"W", startingCoordinate: marsrover.Coordinate{Point:marsrover.Point{X:3,Y:3}, Direction:"W"}, finalCoordinate: marsrover.Coordinate{Point: marsrover.Point{X:2,Y:3}, Direction: "W"}},
		{direction:"S", startingCoordinate: marsrover.Coordinate{Point:marsrover.Point{X:4,Y:2}, Direction:"S"}, finalCoordinate: marsrover.Coordinate{Point: marsrover.Point{X:4,Y:1}, Direction: "S"}},
		{direction:"N", startingCoordinate: marsrover.Coordinate{Point:marsrover.Point{X:3,Y:3}, Direction:"N"}, finalCoordinate: marsrover.Coordinate{Point: marsrover.Point{X:3,Y:4}, Direction: "N"}},

	}

	for _, tt := range areaTestsNotEdgePoints{
		t.Run("Not an edge point/" + tt.direction, func(t *testing.T){
			got:= s.MoveForward(&tt.startingCoordinate)

			if *got!= tt.finalCoordinate{
				t.Errorf("got %+v want %+v", got, tt.finalCoordinate)
			}
		})

	}

	areaTestsEdgePoints := []struct{
		direction string
		startingCoordinate marsrover.Coordinate
		finalCoordinate marsrover.Coordinate
	}{
		{direction:"E", startingCoordinate: marsrover.Coordinate{Point:marsrover.Point{X:5,Y:3}, Direction:"E"}, finalCoordinate: marsrover.Coordinate{Point: marsrover.Point{X:1,Y:3}, Direction: "E"}},
		{direction:"W", startingCoordinate: marsrover.Coordinate{Point:marsrover.Point{X:1,Y:3}, Direction:"W"}, finalCoordinate: marsrover.Coordinate{Point: marsrover.Point{X:5,Y:3}, Direction: "W"}},
		{direction:"S", startingCoordinate: marsrover.Coordinate{Point:marsrover.Point{X:4,Y:1}, Direction:"S"}, finalCoordinate: marsrover.Coordinate{Point: marsrover.Point{X:4,Y:5}, Direction: "S"}},
		{direction:"N", startingCoordinate: marsrover.Coordinate{Point:marsrover.Point{X:3,Y:5}, Direction:"N"}, finalCoordinate: marsrover.Coordinate{Point: marsrover.Point{X:3,Y:1}, Direction: "N"}},
	}

	for _, tt := range areaTestsEdgePoints{
		t.Run("Edge point/" + tt.direction, func(t *testing.T){
			got:= s.MoveForward(&tt.startingCoordinate)

			if *got!= tt.finalCoordinate{
				t.Errorf("got %+v want %+v", got, tt.finalCoordinate)
			}
		})

	}
	
}

func TestMoveBackward(t *testing.T){
	s := marsrover.NewService()
	areaTestsNotEdgePoints:= []struct{
		direction string
		startingCoordinate marsrover.Coordinate
		finalCoordinate marsrover.Coordinate
	}{
		{direction:"E", startingCoordinate: marsrover.Coordinate{Point:marsrover.Point{X:2,Y:3}, Direction:"E"}, finalCoordinate: marsrover.Coordinate{Point: marsrover.Point{X:1,Y:3}, Direction: "E"}},
		{direction:"W", startingCoordinate: marsrover.Coordinate{Point:marsrover.Point{X:3,Y:3}, Direction:"W"}, finalCoordinate: marsrover.Coordinate{Point: marsrover.Point{X:4,Y:3}, Direction: "W"}},
		{direction:"S", startingCoordinate: marsrover.Coordinate{Point:marsrover.Point{X:4,Y:2}, Direction:"S"}, finalCoordinate: marsrover.Coordinate{Point: marsrover.Point{X:4,Y:3}, Direction: "S"}},
		{direction:"N", startingCoordinate: marsrover.Coordinate{Point:marsrover.Point{X:3,Y:3}, Direction:"N"}, finalCoordinate: marsrover.Coordinate{Point: marsrover.Point{X:3,Y:2}, Direction: "N"}},

	}

	for _, tt := range areaTestsNotEdgePoints{
		t.Run("Not an edge point/" + tt.direction, func(t *testing.T){
			got:= s.MoveBackward(&tt.startingCoordinate)

			if *got!= tt.finalCoordinate{
				t.Errorf("got %+v want %+v", got, tt.finalCoordinate)
			}
		})

	}

	areaTestsEdgePoints := []struct{
		direction string
		startingCoordinate marsrover.Coordinate
		finalCoordinate marsrover.Coordinate
	}{
		{direction:"E", startingCoordinate: marsrover.Coordinate{Point:marsrover.Point{X:1,Y:3}, Direction:"E"}, finalCoordinate: marsrover.Coordinate{Point: marsrover.Point{X:5,Y:3}, Direction: "E"}},
		{direction:"W", startingCoordinate: marsrover.Coordinate{Point:marsrover.Point{X:5,Y:3}, Direction:"W"}, finalCoordinate: marsrover.Coordinate{Point: marsrover.Point{X:1,Y:3}, Direction: "W"}},
		{direction:"S", startingCoordinate: marsrover.Coordinate{Point:marsrover.Point{X:4,Y:5}, Direction:"S"}, finalCoordinate: marsrover.Coordinate{Point: marsrover.Point{X:4,Y:1}, Direction: "S"}},
		{direction:"N", startingCoordinate: marsrover.Coordinate{Point:marsrover.Point{X:3,Y:1}, Direction:"N"}, finalCoordinate: marsrover.Coordinate{Point: marsrover.Point{X:3,Y:5}, Direction: "N"}},
	}

	for _, tt := range areaTestsEdgePoints{
		t.Run("Edge point/" + tt.direction, func(t *testing.T){
			got:= s.MoveBackward(&tt.startingCoordinate)

			if *got!= tt.finalCoordinate{
				t.Errorf("got %+v want %+v", got, tt.finalCoordinate)
			}
		})

	}
}

func TestTurnLeft(t *testing.T){
	s := marsrover.NewService()
	areaTest := []struct{
		startingCoordinate marsrover.Coordinate
		finalCoordinate marsrover.Coordinate
	}{
		{startingCoordinate: marsrover.Coordinate{Point: marsrover.Point{X:4,Y:2}, Direction: "E"},finalCoordinate: marsrover.Coordinate{Point: marsrover.Point{X:4,Y:2}, Direction: "N"}},
		{startingCoordinate: marsrover.Coordinate{Point: marsrover.Point{X:4,Y:2}, Direction: "N"},finalCoordinate: marsrover.Coordinate{Point: marsrover.Point{X:4,Y:2}, Direction: "W"}},
		{startingCoordinate: marsrover.Coordinate{Point: marsrover.Point{X:3,Y:2}, Direction: "W"},finalCoordinate: marsrover.Coordinate{Point: marsrover.Point{X:3,Y:2}, Direction: "S"}},
		{startingCoordinate: marsrover.Coordinate{Point: marsrover.Point{X:4,Y:2}, Direction: "S"},finalCoordinate: marsrover.Coordinate{Point: marsrover.Point{X:4,Y:2}, Direction: "E"}},
	}

	for _, tt := range areaTest{
		t.Run(tt.startingCoordinate.Direction, func(t *testing.T){
			got:= s.TurnLeft(&tt.startingCoordinate)

			if *got!= tt.finalCoordinate{
				t.Errorf("got %+v want %+v", got, tt.finalCoordinate)
			}
		})

	}
}

func TestTurnRight(t *testing.T){
	s := marsrover.NewService()
	areaTest := []struct{
		startingCoordinate marsrover.Coordinate
		finalCoordinate marsrover.Coordinate
	}{
		{startingCoordinate: marsrover.Coordinate{Point: marsrover.Point{X:4,Y:2}, Direction: "E"},finalCoordinate: marsrover.Coordinate{Point: marsrover.Point{X:4,Y:2}, Direction: "S"}},
		{startingCoordinate: marsrover.Coordinate{Point: marsrover.Point{X:4,Y:2}, Direction: "N"},finalCoordinate: marsrover.Coordinate{Point: marsrover.Point{X:4,Y:2}, Direction: "E"}},
		{startingCoordinate: marsrover.Coordinate{Point: marsrover.Point{X:3,Y:2}, Direction: "W"},finalCoordinate: marsrover.Coordinate{Point: marsrover.Point{X:3,Y:2}, Direction: "N"}},
		{startingCoordinate: marsrover.Coordinate{Point: marsrover.Point{X:4,Y:2}, Direction: "S"},finalCoordinate: marsrover.Coordinate{Point: marsrover.Point{X:4,Y:2}, Direction: "W"}},
	}

	for _, tt := range areaTest{
		t.Run(tt.startingCoordinate.Direction, func(t *testing.T){
			got:= s.TurnRight(&tt.startingCoordinate)

			if *got!= tt.finalCoordinate{
				t.Errorf("got %+v want %+v", got, tt.finalCoordinate)
			}
		})

	}
}

func TestCommand(t *testing.T){
	s := marsrover.NewService()
	testArea := []struct{
		definition string
		startingCoordinate marsrover.Coordinate
		commandList []string
		want marsrover.Coordinate
		wantError error
	}{
		{definition: "Road with obstacle 1", startingCoordinate: marsrover.Coordinate{Point: marsrover.Point{X: 2, Y: 3}, Direction: "E"}, commandList : []string{"l", "r", "f","f","f"}, want : marsrover.Coordinate{Point : marsrover.Point{X: 3, Y:3}, Direction: "E"}, wantError : marsrover.ErrObstacleOnRoad},
		{definition: "Road with obstacle 2", startingCoordinate: marsrover.Coordinate{Point: marsrover.Point{X: 4, Y: 3}, Direction: "E"}, commandList : []string{"l","f","f","l","f","f","f"}, want : marsrover.Coordinate{Point : marsrover.Point{X: 3, Y:5}, Direction: "W"}, wantError : marsrover.ErrObstacleOnRoad},
		{definition: "Road without obstacle 1", startingCoordinate: marsrover.Coordinate{Point: marsrover.Point{X: 2, Y: 3}, Direction: "E"}, commandList : []string{"l", "r", "f", "r","r","f","f"}, want : marsrover.Coordinate{Point : marsrover.Point{X: 1, Y:3}, Direction: "W"}, wantError : marsrover.ErrNoObstacleOnRoad},
	}
	for _, tt := range testArea{
		t.Run(tt.definition, func(t *testing.T){
			got, err := s.Command(&tt.startingCoordinate, tt.commandList)

			if *got != tt.want || err != tt.wantError {
				t.Errorf("got coorrdinate %+v want coordinate %+v ;;;;; got error %t want error %t", got, tt.want, err, tt.wantError )
			}
		})
	}
}

func TestObstacleDetection(t *testing.T){
	s:= marsrover.NewService()

	areaTest := []struct{
		obstacle bool
		coordinate marsrover.Coordinate
	}{
		{obstacle: true, coordinate: marsrover.Coordinate{Point : marsrover.Point{X: 4, Y: 3}, Direction: "E" }},
		{obstacle: true, coordinate: marsrover.Coordinate{Point : marsrover.Point{X: 2, Y: 5}, Direction: "W" }},
		{obstacle: false, coordinate: marsrover.Coordinate{Point : marsrover.Point{X: 1, Y: 5}, Direction: "E" }},

	}

	for _, tt := range areaTest{
		t.Run("obstacle" + strconv.FormatBool(tt.obstacle), func(t *testing.T){
			check := s.ObstacleDetection(&tt.coordinate)

			if check != tt.obstacle{
				t.Errorf("got %t want %t", check, tt.obstacle)
			}
		})
	}

}

func TestCheckIfTheInitialCoordinateIsValid(t *testing.T){
	s:= marsrover.NewService()

	areaTest := []struct{
		definition string
		coordinate marsrover.Coordinate
		want bool
	}{
		{definition: "outside of coordinate system", coordinate: marsrover.Coordinate{
			Point: marsrover.Point{X: 6, Y: 3},
			Direction: "E"},
			want: false,
		},
		{definition: "inside the coordinate system", coordinate: marsrover.Coordinate{
			Point: marsrover.Point{X: 2, Y: 3},
			Direction: "E"},
			want: true,
		},

	}
	for _, tt := range areaTest{
		t.Run(tt.definition, func(t *testing.T){
			check := s.CheckIfTheInitialCoordinateIsValid(&tt.coordinate)
			assert.Equal(t, tt.want, check)
		})
		
	}
}

func TestGetResult(t *testing.T){
	s:= marsrover.NewService()

	areaTest:= []struct{
		definition string
		toCheck marsrover.MarsRoverRequest
		want marsrover.Response
	}{
		{
			definition: "When the coordinate is outside of the coordinate system",
			toCheck: marsrover.MarsRoverRequest{
				Coordinate: marsrover.Coordinate{
					Point: marsrover.Point{
						X: 6,
						Y: 3,
					},
					
				},
			},
			want: marsrover.Response{
				ErrorResponse: marsrover.ErrorResponse{
					Error: marsrover.ErrNotValidInitialPoint.Error(),
				},
				IsThereError: true,
			},
		},
		{
			definition: "When there is an obstacle on the given point",
			toCheck: marsrover.MarsRoverRequest{
				Coordinate: marsrover.Coordinate{
					Point: marsrover.Point{
						X: 2,
						Y: 5,
					},
					
				},

			},
			want: marsrover.Response{
				ErrorResponse: marsrover.ErrorResponse{
					Error: marsrover.ErrLocatedOnObstacle.Error(),
				},
				IsThereError: true,
			},
		},
		{
			definition: "When initial point is valid and there is an obstacle on the road",
			toCheck: marsrover.MarsRoverRequest{
				Coordinate: marsrover.Coordinate{
					Point: marsrover.Point{
						X: 2,
						Y: 3,
					},
					Direction: "E",
				},
				Commands: []string{"l", "r", "f","f","f"},
			},
			want: marsrover.Response{
				MarsRoverResponse: marsrover.MarsRoverResponse{
					Coordinate: marsrover.Coordinate{
						Point : marsrover.Point{
							X: 3,
							Y:3},
						Direction: "E",
					},
					Message: marsrover.ErrObstacleOnRoad.Error(),
				},
				IsThereError: false,
			},
		},
		{
			definition: "When initial point is valid and there is no obstacle on the road",
			toCheck: marsrover.MarsRoverRequest{
				Coordinate: marsrover.Coordinate{
					Point: marsrover.Point{
						X: 2,
						Y: 3,
					},
					Direction: "E",
				},
				Commands: []string{"l", "r", "f", "r","r","f","f"},
			},
			want: marsrover.Response{
				MarsRoverResponse: marsrover.MarsRoverResponse{
					Coordinate: marsrover.Coordinate{
						Point : marsrover.Point{
							X:1,
							Y:3,
						},
						Direction: "W",
					},
					Message: marsrover.ErrNoObstacleOnRoad.Error(),
				},
				IsThereError: false,
			},
		},
	}
	for _, tt := range areaTest{
		t.Run(tt.definition, func(t *testing.T){
			response := s.GetResult(&tt.toCheck)
			assert.Equal(t, tt.want, *response)
		})
	}
}