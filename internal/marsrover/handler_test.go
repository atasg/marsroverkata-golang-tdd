package marsrover_test

import (
	"strings"
	"marsRover/internal/marsrover"
	"testing"
	mocks "marsRover/internal/mocks/marsrover"
	"github.com/gofiber/fiber/v2"
	"encoding/json"
	"net/http/httptest"
	"net/http"
	"bytes"
	"io/ioutil" 
	gomock "github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
)

func CreateMockApp(t *testing.T) (*fiber.App, *mocks.MockHandlerService){
	app := fiber.New()
	mockCtrl:= gomock.NewController(t)
	defer mockCtrl.Finish()
	mockService := mocks.NewMockHandlerService(mockCtrl)
	handler := marsrover.NewHandler(mockService)
	handler.SetupRoutes(app)
	return app, mockService
}

const (
	newCoordinatePath = "/marsrover/newcoordinate"
	healthCheckPath = "/marsrover/health"
	badRequest = `{"message":"Failed to parse body","success":false}`
)

func TestHealthCheck(t *testing.T){
	mockApp, _ :=CreateMockApp(t)
	request := httptest.NewRequest(http.MethodGet, healthCheckPath, http.NoBody)
	request.Header.Add("Content-Type", "application/json")
	
	response, _ :=mockApp.Test(request)

	assert.Equal(t, http.StatusOK, response.StatusCode)
}

func TestBadRequest(t *testing.T){
	mockApp, _ := CreateMockApp(t)

	r := strings.NewReader("bad request")
	request := httptest.NewRequest(http.MethodGet, newCoordinatePath, r)
	request.Header.Add("Content-Type", "application/json")

	response, _ := mockApp.Test(request)

	assert.Equal(t, http.StatusBadRequest, response.StatusCode)

	body, _:= ioutil.ReadAll(response.Body)

	assert.Equal(t,badRequest,string(body))
}

 func TestWhenGivenPointIsNotInCoordinateSystem(t *testing.T){
	mockRequest := marsrover.MarsRoverRequest{
		Coordinate: marsrover.Coordinate{
			Point: marsrover.Point{
				X: 6,
				Y: 3,
			},
			
		},
	}
	mockResponse := marsrover.Response{
		ErrorResponse: marsrover.ErrorResponse{
			Error: marsrover.ErrNotValidInitialPoint.Error(),
		},
		IsThereError: true,
	}

	requestBody, err := json.Marshal(mockRequest)
	if err != nil {
		t.Fatal("can not Marshall request body")
	}
	mockApp, mockService := CreateMockApp(t)

	mockService.EXPECT().GetResult(&mockRequest).Return(&mockResponse)

	request := httptest.NewRequest(http.MethodGet, newCoordinatePath, bytes.NewBuffer(requestBody))
	request.Header.Add("Content-Type", "application/json")
	response, _ := mockApp.Test(request)
	body, _ := ioutil.ReadAll(response.Body)

	var got marsrover.ErrorResponse
	json.Unmarshal(body, &got)

	assert.Equal(t, http.StatusUnprocessableEntity, response.StatusCode)
	assert.Equal(t, mockResponse.ErrorResponse, got)
} 

func TestWhenGivenPointIsValid(t *testing.T){
	mockRequest := marsrover.MarsRoverRequest{
		Coordinate: marsrover.Coordinate{
			Point: marsrover.Point{
				X: 2,
				Y: 3,
			},
			Direction: "E",
		},
		Commands: []string{"l", "r", "f","f","f"},
	}
	mockResponse := marsrover.Response{
		MarsRoverResponse: marsrover.MarsRoverResponse{
			Coordinate: marsrover.Coordinate{
				Point : marsrover.Point{
					X: 3,
					Y:3},
				Direction: "E",
			},
			Message: marsrover.ErrObstacleOnRoad.Error(),
		},
		IsThereError: false,
	}

	requestBody, err := json.Marshal(mockRequest)
	if err != nil {
		t.Fatal("can not Marshall request body")
	}
	mockApp, mockService := CreateMockApp(t)

	mockService.EXPECT().GetResult(&mockRequest).Return(&mockResponse)

	request := httptest.NewRequest(http.MethodGet, newCoordinatePath, bytes.NewBuffer(requestBody))
	request.Header.Add("Content-Type", "application/json")
	response, _ := mockApp.Test(request)
	body, _ := ioutil.ReadAll(response.Body)

	var got marsrover.MarsRoverResponse
	json.Unmarshal(body, &got)

	assert.Equal(t, http.StatusOK, response.StatusCode)
	assert.Equal(t, mockResponse.MarsRoverResponse, got)
} 
