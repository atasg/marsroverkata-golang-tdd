package marsrover

import(
	"errors"
)

type Service struct{
}

func NewService() *Service {
	service := Service{}
	return &service
}

func (s *Service) MoveForward(c *Coordinate) *Coordinate{
	direction:= c.Direction
	switch direction{
	case "E":
		c.Point.X +=1
	case "W":
		c.Point.X -=1
	case "S":
		c.Point.Y -=1
	case "N":
		c.Point.Y +=1
	}
	xCoordinate := c.Point.X 
	switch xCoordinate{
	case 0:
		c.Point.X = 5
	case 6:
		c.Point.X = 1
	}

	yCoordinate := c.Point.Y
	switch yCoordinate{
	case 0:
		c.Point.Y = 5
	case 6:
		c.Point.Y = 1
	}
	
	return c
} 

func (s *Service) MoveBackward(c *Coordinate) *Coordinate{
	direction:= c.Direction
	switch direction{
	case "E":
		c.Point.X -=1
	case "W":
		c.Point.X +=1
	case "S":
		c.Point.Y +=1
	case "N":
		c.Point.Y -=1
	}
	xCoordinate := c.Point.X 
	switch xCoordinate{
	case 0:
		c.Point.X = 5
	case 6:
		c.Point.X = 1
	}

	yCoordinate := c.Point.Y
	switch yCoordinate{
	case 0:
		c.Point.Y = 5
	case 6:
		c.Point.Y = 1
	}
	
	return c
}

func (s *Service) TurnLeft(c *Coordinate) *Coordinate{
	direction := c.Direction
	switch direction{
	case "E":
		c.Direction = "N"
	case "W":
		c.Direction = "S"
	case "S":
		c.Direction = "E"
	case "N":
		c.Direction = "W"
	}
	return c
}

func (s *Service) TurnRight(c *Coordinate) *Coordinate{
	direction := c.Direction
	switch direction{
	case "E":
		c.Direction = "S"
	case "W":
		c.Direction = "N"
	case "S":
		c.Direction = "W"
	case "N":
		c.Direction = "E"
	}
	return c
}

var obstaclePoints = [2]Point{
	Point{X : 4, Y: 3},
	Point{X : 2, Y: 5},
}

var (
	ErrObstacleOnRoad = errors.New("there is an obstacle on the road. Rover can go no further")
	ErrNoObstacleOnRoad = errors.New("there is no obstacle on the road")
	ErrLocatedOnObstacle = errors.New("the rover can not be located on given coordinates. There is an obstacle on the given coordinate")
	ErrNotValidInitialPoint = errors.New("the rover can not be located on given coordinate. Edge coordinate is 5 for X and Y axis")
)

func (s *Service) ObstacleDetection(c *Coordinate) bool{
	pointToCheck := c.Point
	for _, point := range obstaclePoints{
		if point == pointToCheck{
			return true
		} 
	}
	return false
}

func (s *Service) CheckIfTheInitialCoordinateIsValid(c *Coordinate) bool{
	point := c.Point
	if point.X > 5 || point.X < 0 || point.Y > 5 || point.Y < 0 {
		return false
	}
	return true
}

func (s *Service) Command(c *Coordinate, commandList []string) (*Coordinate, error){
	for _ ,command := range commandList{
		switch command{
		case "l":
			c = s.TurnLeft(c)
		case "r":
			c = s.TurnRight(c)
		case "f":
			c = s.MoveForward(c)
			if s.ObstacleDetection(c) {
				c = s.MoveBackward(c)
				return c, ErrObstacleOnRoad
			}
			
		case "b":
			c = s.MoveBackward(c)
			if s.ObstacleDetection(c) {
				c = s.MoveForward(c)
				return c, ErrObstacleOnRoad
			}
		}
	}
	return c, ErrNoObstacleOnRoad
}

func (s *Service) GetResult(r *MarsRoverRequest) *Response{
	initialCoordinate :=r.Coordinate
	commands:=r.Commands

	if !s.CheckIfTheInitialCoordinateIsValid(&initialCoordinate){
		response := ErrorResponse{
			Error: ErrNotValidInitialPoint.Error(),
		}
		return &Response{
			ErrorResponse: response,
			IsThereError: true,
		}
	}

	if s.ObstacleDetection(&initialCoordinate){
		response := ErrorResponse{
			Error: ErrLocatedOnObstacle.Error(),
		}
		return &Response{
			ErrorResponse: response,
			IsThereError: true,
		}
	}

	finalCoordinate, err := s.Command(&initialCoordinate,commands)
	response := MarsRoverResponse{
		Coordinate: *finalCoordinate,
		Message: err.Error(),
	}
	return &Response{
		MarsRoverResponse: response,
		IsThereError: false,
	}
}