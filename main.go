package main

import (
	"marsRover/internal/marsrover"
	"github.com/gofiber/fiber/v2"
)

func CreateApp() *fiber.App{
	service := marsrover.NewService()
	handler := marsrover.NewHandler(service)
	app := fiber.New()
	handler.SetupRoutes(app)
	return app
}


func main(){
	app:=CreateApp()
	app.Listen(":8080")
}
